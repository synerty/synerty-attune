
.. _upgrade_notes:

=============
Upgrade Notes
=============

.. toctree::
    :maxdepth: 1
    :caption: Contents:

    v2.1.x/v2.1.x
