.. _attune_help_archives:

========
Archives
========

All files stored in Attune are stored as Archives - even single files.  Archives are used in Procedures to deploy
any files needed, such as installers, packages, configuration files.  When you access the Archive screen from Attune's navigation panel at the top of the screen,
you will be presented with a screen with two panels as shown below.

.. image:: doc.archive.screen1.png

:1: The left hand panel shows a list of the archives that have already been created

:2: Add button allows you to create a new Archive

:3: View and edit the attributes of the Archive

Creating an Archive
-------------------

If the right hand pane has 'New Archive' at the top, with blank fields, you can create a new archive
by entering the name and other details in the boxes provided.  You can also click on the 'Add' button
on the top of the left hand pane.

.. image:: doc.archive.screen2.png

You will need to specify what archive format you wish to create.  Archives can be in zip, tar, gz or bz2 formats


You will also need to specify the Archive type.  There are three different types of Archives, depending on the
types of files they will contain.

:Generic Archive: Any files that are not template files or Red Hat Package .rpm files

:Redhat Packages: .rpm files for installing and updating software

:Template Files: Special Archives used by Attune's Template engine


When you click the 'Create' button at the bottom the new archive will be created and the screen will change,
giving you more options.

.. image:: doc.archive.screen3.png

:1: Button to lock or unlock the Archive.  You will need to unlock the Archive to
             add files to the Archive

:2: Add/Update area.  Click and drag files here to add them to Archive

:3: Replace area.  Click and drag files here to replace all archive contents
              with new file

:4: Area showing list of files in Archive.  Button to create new file in Archive
             using Attune's built in editor.

:5: Save, Reset, Delete Buttons : Saves changes to Archive, Resets unsaved changes to Archive,
             Deletes the entire Archive.

Adding files to an Archive
--------------------------

There are three ways to add files to a newly created Attune Archive

You can click and drag a selection of files, or an existing archive file, into the 'Replace' area at the bottom left
of the screen.  Archive files in .zip, .tar, .tar.gz or .tar.bz2 formats can be added into the Attune archive.
Any existing files in the Archive will be removed and replaced with the files you have just added.

You can click and drag files into the 'Add/Update' area at the bottom of the screen.  These files will be added
without affecting any other files in the Archive

You can create files with Attune's built in text editor, which supports syntax highlighting for a wide variety of
scripting, programming and markup languages.  Click on the 'Create File+' button and the editor screen will appear

.. image:: doc.archive.screen4.png

You will need to give your file a name.  You can also specify a path, syntax highlighting, and linux or windows
line end options.

The path is optional.  Specifying a path to a directory or folder will not create the file in that folder
or directory.
Files created with Attune's built in editor exist only in the Attune Archive.

The syntax option will default to text if no other option is provided.  The drop-down box allows you to
choose syntax highlighting options for a variety of scripting languages and text based formats.  The editor
cannot edit binary files, or files larger than 100mb.

On the right, just above the contents, is an 'Editor Help' button.  This brings up a dialog showing
keyboard shortcuts for the editor, with key bindings for pc/linux and mac keyboards.  The keyboard shortcuts
give the editor added functionality for editing source code files.

The line end option defaults to linux unless you specify windows.

The Contents area is where the contents of your file are displayed and edited.To save your additions
or changes, click on the 'Create' or 'Save' button at the bottom right of the editor dialog.

Editing an Archive
------------------

When you open an Archive you will see a list of the files contained in that Archive just above the area
where you drag files into the Archive.  If they are text files or script files they can be opened and
edited with Attune's built in editor by clicking on the 'open' button to the left of the filename.

.. image:: doc.archive.screen5.png

To remove a file from the archive, open the file by clicking on the 'open' button near the filename.
When the editor dialog opens, click on the red 'Delete' button at the bottom right of the editor dialog
and the file will be removed from the Archive.

The red 'Delete' button above the list of files in the Archive will delete the entire Archive.  Don't use
this to remove a file from the Archive because the Archive will be deleted.

Deleting an Archive
-------------------

To delete an Archive, first select the Archive you wish to delete from the left hand pane in the
Archives screen.  This will display the details of the Archive in the right hand pane.

To permanently delete the Archive from Attune, click on the red 'Delete' button in the mid-left of the
right hand pane.











