.. _attune_help_config_archives:

======================
Configuration Archives
======================


Attune has a template engine that allows you to deploy files which contain markup
using Mako, an embedded Python language.
The template engine processes the files in a configuration archive and renders the
final outputs.


More information about Mako templates, and the syntax used for the configuration
archives files can be found at
`<http://www.makotemplates.org>`_


Creating a Configuration Archive
--------------------------------


All files in Attune, including configuration files, are stored in Archives.
Create a new Template in Attune's Archives screen,
give it a name, and select the 'Template Files' type from the drop down list.
Click the 'Create' button to create the
Archive. Make sure it is unlocked and able to be modified.



.. image:: doc.archive.config.screen3.png


:1: Name of the new Template Archive

:2: Lock / Unlock button. Make sure Archive is unlocked and
                contents are editable

:3: Archive Type list - select 'Template Files'

:4: Button to create file with Attune's editor.
    You can create
    your template file, or any other script or text file
    and save it in the current Archive.

:5: Area to drop files into Archive.
    Template files, any other
    files, created with an external editor can be
    dropped into this area to add them to the Archive.

:6: List of files contained in the Archive.
    You can click on the
    'Open' button on the left to view or edit
    the individual files with Attune's built in editor.

:7: Buttons to Save changes to Archive, Reset changes made
    since previous save, or Delete the Archive


The Attune editor is the easiest way of creating the template file and adding it
to
your archive. It can also create any other program,
script, or text based files. Your template file will need a name. The path, which
is
optional, is where the file will be deployed to
after it has been processed by the template engine. Add your template code in the
'Contents' area and click on the 'Create' button at
the bottom to create the template file and add it to the archive.



.. image:: doc.archive.config.screen2.png


Elements in the code such as  **${server.ip}** are not variables -
they
are elements of the
mako template language, that will be processed by the template engine. When a
procedure step is created that deploys the
template archive, these parameters will be assigned to Attune Variables.
Values will the be assigned to the Variables in
a Plan, prior to job. Template Archive files can also contain other
non-template
files. These files will not be processed
by Attune's template engine, and will be deployed normally.


When you have created, or added, a template file to the Template Archive it will
appear in the list of files. Click on the 'Save'
button to save any changes you have made to the Archive.


Deploying a template Archive in a Procedure
-------------------------------------------


Templates are deployed in Attune by using a 'Deploy Template Archive' step in an
Attune procedure. Create, or select a
procedure that you want to deploy a Template Archive, and click the 'Add' button
to
create a new Procedure step.
The details for the new step will appear on the right pane. At the top there is a
drop
down box showing the type of
Procedure step - this will default to 'Execute Shell Step'. Change this to 'Deploy
Template Archive' in the drop down list,
and some additional options will appear



You will need to select Server and OS Credential Variables from the drop down
list.
This specifies the Server and a login
for the Server where the template will be deployed. You will need Variables of
the
correct types ( Server and OS Credential )
defined so they can be selected. The variables serve to hold a value - the
actual
values (values) will be assigned to these
variables in a Plan before the Procedure is executed. As long as you assign
variables of the appropriate type to the
parameters in the template file, you will be able to assign whatever values you
need
to them in the Plan.



The Archive containing the template is also selected from a drop down box - so it
must
be created in the Archives screen
first. When the Archive is selected, more details are displayed - including a list
of
the files contained in the Archive.


.. image:: doc.archive.config.screen4b.png


:1: **Step Name** - name of the Procedure Step that deploys the
    Template Archive

:2: **Server** - The server the Template Archive is to be
    deployed on

:3: **OS Credential** - OS Credential (userid/password) for the
    Server the Template Archive will be deployed on

:4: **Name of Template Archive File**
    Selected from a drop-down list. Template Archives must be created first in
    the
    Archives screen before they
    can be selected.

:5: **Deploy Path** - optional path for file that has been
    rendered
    by the Template Engine.

:6: List of files contained in the Template Archive File.
    This list will appear after the Template Archive File has been selected.
    You
    can click on the 'Open'
    button on the left to view or edit the individual files with Attune's
    built in
    editor.

:7: Map Template parameters to Values
    The Template file in the Archive uses the Mako template
    engine, which is based on Python. The template file can use variables, and
    you
    may need to be able to assign
    values to these Template Variables. This area is where you can assign
    Attune
    Placeeholders to the Mako template
    variables used in the Template file.

:8: Button to test Template File Generation
    This button allows you to test the operation of the template file - to see
    if
    the template file is processed
    by the template engine. If there are problems or errors generated during
    the
    test, the template file can be
    edited with Attune's built in editor by clicking on the 'Open' button to
    the
    left of the template file. This
    allows you to make changes or alterations to your template file and test
    the
    results while you are working
    on your Procedure steps.

:9: Optional comment, descriptions, instructions, etc

:10: Buttons to:

        *   Save Changes
        *   Reset changes back to last save
        *   Unlink ( Delete ) the current step from the main procedure



When the Template File has been tested and you are satisfied that it runs as
required,
click the 'Save' button to
save your Procedure step. To run your procedure and deploy the Template Archive
you
will need to create a Plan,
and assign Values to the Variables you have used in your procedure. The
plan
can then be run from the
Attune Dashboard, or from a Job.


Packaging Template with a Procedure for Export
----------------------------------------------

You can export a procedure to a file with a .atp extension which can be then
distributed to other people. They can
then import the procedure into their Attune installations. When you export a
procedure
you have the option to also
include any of the Archives and Values that are used in the procedure.



When you click on the 'Export' button to export your procedure, a dialog appears
which
shows the Archives and Values
used in the procedure. The Archives and Values are on separate tabs. Check
the
box to the left of the Archives
and Variables you wish to include in your exported procedure, and then click
the
'Export' button to create the file
containing your procedure, and any Archives and Values you have included with
it.



.. image:: doc.archive.config.screen5.png




When you click on the 'Export' button to export your procedure, a dialog appears
which
shows the Archives and Values
used in the procedure. The Archives and Values are on separate tabs. Check
the
box to the left of the Archives
and Variables you wish to include in your exported procedure, and then click
the
'Export' button to create the file
containing your procedure, and any Archives and Values you have included with
it.



When the procedure is run, the mako in the configuration file is processed by the
template engine, and the output is generated.
For example, the mako code shown in the code window above could be rendered as -



.. image:: doc.archive.config.screen6a.png




Templates are a powerful feature of Attune - you can write blocks of Python code
in a
template. They combine well with
other features of Attune, such as Procedures, Variables and Values to
provide
substantial power and flexibility.
You can also package a template and a procedure to process the template, into a
single
file which can be easiy distributed.






