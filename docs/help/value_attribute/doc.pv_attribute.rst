.. _attune_help_values_in_scripts:

=============================
Referencing values in Scripts
=============================


All Variables have the same basic attributes regardless of their type. The
Values that are mapped onto
the Variables in the Plan are different - they have a number of different
attributes depending on the type
of value.


Using Variables in Procedures
-----------------------------


Variables are used in Procedures to refer to values. These values are held in
Values which are mapped onto the
relevant Variables in a Plan prior to the Procedure being executed.



Procedures are composed of steps, and each step has a unique name and a number of
other attributes, including a script
section, where the actual commands for executing the step are written. In these
commands, curly braces { } are used
to delimit values which are referenced by variables.



In order to be used in a script the Variable names are stripped of any
separating spaces between words and converted
to camelcase - with only the first letter of the second, and any subsequent words,
capitalised.


So a Variable called -  **THIS IS my name** would be converted to
:code:`thisIsMyName`

Values are referenced in scripts as
:code:`{placeHolderName.placeValueAttribute}`



So for a Variable with name  **THIS IS my name** of type  **Server**
We could refer to the IP address in a script as
:code:`{thisIsMyName.ip}`


The expressions contained in the curly braces are evaluated prior to job,
and the curly brace expressions are
replaced by actual values before the script is run. In the example above
:code:`{thisIsMyName.ip}`
would be
substituted with the value of the IP attribute of the PlaceValue that was assigned
to the Variable
**THIS IS my name** in the Plan.

This substitution of values is performed by Attune, so using the curly Brace {}
notation to reference attribute values
works with all scripts regardless of the type of script - it will work with Bash,
Windows Powershell, Python, etc.


Referencing value attributes in Steps
---------------------------------------


OS Credential
`````````````
Attributes of Type OS credential can be referenced as follows:

==============  ==============  =====================================
Variable        Value           Script Reference
==============  ==============  =====================================
Name            name            :code:`{myPlaceHolder.name}`
User            user            :code:`{myPlaceHolder.user}`
Password        password        :code:`{myPlaceHolder.password}`
Comment         comment         :code:`{myPlaceHolder.comment}`
==============  ==============  =====================================


SQL Credential
``````````````

Attributes of Type SQL credential can be referenced as follows :

==============  ==============  =====================================
Variable        Value           Script Reference
==============  ==============  =====================================
Name            name            :code:`{myPlaceHolder.name}`
User            user            :code:`{myPlaceHolder.user}`
SID             sid             :code:`{myPlaceHolder.sid}`
Password        password        :code:`{myPlaceHolder.password}`
As Sysdba       asSysDba        :code:`{myPlaceHolder.asSysDba}`
Comment         comment         :code:`{myPlaceHolder.comment}`
==============  ==============  =====================================



Server
``````

Attributes of Server are referenced as follows:

==============  ==============  =====================================
Variable        Value           Script Reference
==============  ==============  =====================================
Name            name            :code:`{myPlaceHolder.name}`
IP Address      ip              :code:`{myPlaceHolder.ip}`
Hostname        hostname        :code:`{myPlaceHolder.hostname}`
Domain Name     domain          :code:`{myPlaceHolder.domain}`
Comment         comment         :code:`{myPlaceHolder.comment}`
==============  ==============  =====================================


Server Group
````````````
Server Group is an comma separated list of Server values (CSV),
Attributes of Server Group are referenced as follows:

==============  ================  ==============================================
Variable        Value             Script Reference
==============  ================  ==============================================
Name            name              :code:`{myPlaceHolder.name}`
Server          serverNames       :code:`{myPlaceHolder.serverNames}` as CSV
Server          serverIps         :code:`{myPlaceHolder.serverIps}` as CSV
Server          serverHostnames   :code:`{myPlaceHolder.serverHostnames}` as CSV
Server          serverTypes       :code:`{myPlaceHolder.serverTypes}` as CSV

                                  The types are:
                                        *   1 = Linux
                                        *   2 = Windows

Server          serverDomains     :code:`{myPlaceHolder.serverDomains}` as CSV
Server          serverFqns        :code:`{myPlaceHolder.serverFqns}` as CSV
Comment         comment           :code:`{myPlaceHolder.comment}`
==============  ================  ==============================================



IPv4 Subnet
```````````

Attributes of the "IPv4 Subnet" value are referenced as follows:

==============  ================  ==============================================
Variable        Value             Script Reference
==============  ================  ==============================================
Name            name              :code:`{myPlaceHolder.name}`
Subnet          subnet            :code:`{myPlaceHolder.subnet}`
Netmask         netmask           :code:`{myPlaceHolder.netmask}`
Gateway IP      gateway           :code:`{myPlaceHolder.gateway}`
DNS IP 1        dns1              :code:`{myPlaceHolder.dns1}`
DNS IP 2        dns2              :code:`{myPlaceHolder.dns2}`
Comment         comment           :code:`{myPlaceHolder.comment}`
==============  ================  ==============================================


Text
````

Attributes of Type Text can be referenced as follows:

==============  ================  ==============================================
Variable        Value             Script Reference
==============  ================  ==============================================
Name            name              :code:`{myPlaceHolder.name}`
Value           value             :code:`{myPlaceHolder.value}`
Comment         comment           :code:`{myPlaceHolder.comment}`
==============  ================  ==============================================

