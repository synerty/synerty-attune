.. _attune_help_variables:

=========
Variables
=========


The procedures that are created and executed in Attune are made up of a series of
steps.

These steps make use of variables which allow values to be dynamically
allocated to each step before the steps are
executed. A Plan links the Procedure Variables to Values. The values
contain the properties that are
accessed when a plan is executed. Each Variable has a unique name, and a type.

When you access the Variables screen from Attune's navigation panel at the top
of the screen,
you will be presented with a screen with two panels as shown below.

.. image:: doc.variable_screen_b.png

:1: The left hand panel shows a list of the variables
            that have already been defined,
            along with their types


:2: At the top of the left panel there is a search box that
            allows you to search for a particular
            variable


:3: Next to the search box there is an Add button that
            allows you to create a new
            variable


:4: The right hand panel is where you can view and edit the
            attributes of the
            variables


Types of Variable
-----------------


Each variable has a type which conforms to one of the variable types defined
in Attune. Variables are
assigned their type when they are created. The different types of variable in
Attune are -

:OS Credential: Supply login details for OS - Name, username, password, etc

:Server: Allows access to Server details - IP address, hostname,
            domain name, OS Type, etc

:Server Group: Allows you to access servers within a defined server group

:SQL Credential: Supply login details for SQL databases

:Text: Supply a pre-defined text value


Variable Attributes
-------------------

All Variables have the same basic attributes, regardless of their type.
These attributes are -


:Type: Type must be one of the types defined in Attune
                (mandatory).
                Variables can only be assigned Values of the same type.

:Name: Name of the Variable (mandatory)

:Default Place Value: Default value selected from drop down list of
                Values
                of same type as Variable

:Show in Plan: If checkbox selected, the Variable will be visible in
                the Plan and can have
                different Variables assigned to it

:Comment: Descriptive comment, notes, etc

If there is a default value, a different value can still be assigned to the
variable in the Plan. If there is no default
value, the Variable will appear in the Plan as blank and will need to have a
Value assigned to it.


Creating new Variable
---------------------

If the right hand pane has 'New Variable' in the titlebar, with blank fields,
you can create a new variable
by adding the required details into the fields provided. Alternatively you can
click on the +Add button in the
top right hand side of the left pane, next to the search box. This will display
the 'New Variable' dialog in the
right hand pane. Supply values for the Variable attributes (as described
above), then click the 'Create' button
at the bottom.



.. image:: doc.variable_add.png


Editing a Variable
------------------

Select one of the Variables listed in the left hand pane by left clicking it.
The fields will be shown in the right
hand pane. The fields can then be altered as required. When complete, click the
'Save' button at the bottom of the
dialog box.


.. image:: doc.variable_edit.png


Deleting Variable
-----------------

Select one of the Variables listed in the left hand pane by left clicking it.
The fields will be shown in the right
hand pane. Then simply click on the 'Delete' button at the bottom of the dialog
box.


Using Variables in Scripts
--------------------------


In order to be used in a script the Variable names are stripped of any
separating spaces between words and converted
to camelcase -  with only the first letter of the second, and any subsequent
words, capitalised.


ie. A Variable called -  **THIS IS my name** would be converted to
:code:`thisIsMyName`

Values are referenced in scripts as :code:`{placeHolderName.placeValueAttribute}`

So, for example, if we had a Variable with name  **THIS IS my name**
of type  **Server**


We could refer to the IP address in a script as
:code:`{thisIsMyName.ip}`



More details about using variables and values in scripts, including a list
of the value attributes,
are available


:ref:`attune_help_values_in_scripts`
