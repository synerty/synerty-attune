.. _attune_help_jobs:

====
Jobs
====

Jobs in Attune provide an environment to execute a Plan which tracks each step as it executes, providing feedback
on whether the steps execute correctly or not.  The Plan contains a Procedure, and a set of Values that have been
assigned to the Variables used in the procedure.


The Jobs screen in Attune is divided into to panes.  The left pane shows a list of Plans that already have
Jobs created for them.  The right pane is where details of the Jobs are displayed and entered.


.. image:: doc.job.screen1.png


:1: Drop down box allows you to display Jobs

        *   Manually created in Jobs screen
        *   Created by Attune's scheduler
        *   Plans created to be displayed on Attune Dashboard

:2: A search box - to search for a previously created job

:3: Add button - click to create a new job

:4: List of previously created jobs - each with

        *   Name of the Plan to be Executed
        *   Button to create a new Job of a Previously executed plan
        *   Drop down button to display list of all jobs of a particular plan
        *   List of individual jobs of the plan, showing date and time of job


:5: List of individual jobs of each plan, shows

        *   Date and time plan was executed
        *   Button to discard a particular Job


:6: Pane to create new Job, or display properties of an existing Job

:7: Elements to provide details for a new Job -

        *   Drop-down list of previously created plan
        *   Optional comment for new job


:8: Create Button - Click to Create Job when details have been added



Creating a Job
--------------


To create a new Job, click on the + button in the top left hand pane, the right hand pane will display a blank
form with 'Create New Job' at the top.  Select the Plan you wish to execute from the drop down list at the top,
and add a comment if you want.  Click on the 'Create' button at the bottom to create your Job.


Running a Plan in a Job
-----------------------


If you chose one of the plans from the list in the left pane by clicking on the name, the Procedure will be displayed in
the right pane, showing the hierarchy of steps that make up the procedure.  You can expand or collapse the step hierarchy
by clicking on the small triangle on the left of each step.



.. image:: doc.job.screen2.png

:1: Buttons at the top to -

        *   Open Plan - to check or alter the Values assigned to Variables used in Procedure
        *   Run the Procedure - start job
        *   Stop - stops the procedure at the current point of job.
            Procedure can be re-started from that point.
        *   Abort - stops procedure and abandons further job



:2: Tree Hierarchy diagram showing steps that make up the current procedure

:3: Button that allows you to download a full report of the job of the current procedure



Click on the 'Run' button at the top to start the job.  The steps will run in sequence as shown in the tree hierarchy
diagram.  The output from each step, and any error messages, will be displayed on the right of the tree hierarchy diagram.



You can stop or pause job by clicking on the 'Stop' button at the top of the pane.  The job can be stopped and re-started
at any point - just click on the 'Run' button again to resume job.  The 'Abort' button will stop and abandon the current job
and you will not be able to restart it.



The 'Open Plan' button will open the plan for the current job allowing you to check or alter the assignment of Values
to the variables used in the Procedure.  If you make any changes to the Plan, remember to click the 'Save' button at the bottom
before you close the window.



If you select a particular step in the procedure, the output and error messages for that particular step will be displayed on the
right.  The name of the step is shown at the top of the output, along with buttons to Run Step and Open Step



.. image:: doc.job.screen3.png




Procedure steps that return an error are shown in red text in the tree hierarchy diagram.  If the procedure step shown in red
is a Group procedure, you can expand the procedure by clicking on the triangle to the left of the procedure to see the procedures
contained in the group step.  The procedure step in the group procedure that is producing the error is shown in red text.
You can expand your way down the procedure hierarchy to find the job step responsible for the error.



The 'Run Step' button allows you to run a single step on its own to check its operation. The 'Open Step' will open the step in Attune's
built in editor, allowing you to check or alter the code in that particular step.  If you modify the step with Attune's editor, you can
then run the step to make sure it now runs as required.





