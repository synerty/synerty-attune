.. _attune_help_step_windows_script:

==============
Windows Script
==============

Attune uses the WinRM remote command protocol to execute scripts on windows.


To enable WinRM and file and print sharing, please see :ref:`setup_winrm_cifs_manually`.
