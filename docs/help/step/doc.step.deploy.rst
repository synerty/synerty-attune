.. _attune_help_step_deploy_archive:

==============
Deploy Archive
==============

Attune deploys archives by decompressing them on the Attune server and then
copying the files over to the remote host.

Deploy to Linux Host
--------------------

When the deploy step uses a value of a server with type "Linux/Unix", Attune
switches to using SSH file deployments.

File deployments to linux hosts are transferred over the SCP/SSH prototol.

Locked TAR archive deployments have a special deployment that allows high
performance deployments. TAR files are streamed over the SSH client and decompressed
/ extracted on the remote host.


This is equivilent to : ::

    cat fileOnAttune.tar.bz2 | ssh user@remoteHost 'tar xvjf - -C deployPath'


Deploy to Windows Host
----------------------

When the deploy step uses a value of a server with type "Windows", Attune
switches to using CIFS (AKA Samba/SMB) file deployments. To enable file and print sharing, please see :ref:`connecting_to_windows_no_domains`.

File deployments are currently supported for windows 7 / Windows 2008R2,
and use Direct hosting of SMB over TCP/IP (port 445),
and not older NetBIOS over TCP/IP (port 139)

To enable WinRM and file and print sharing, please see :ref:`setup_winrm_cifs_manually`.
