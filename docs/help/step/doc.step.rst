.. _attune_help_steps:

=====
Steps
=====


.. toctree::
    :maxdepth: 1

    doc.step.deploy
    doc.step.ssh_command
    doc.step.ssh_interactive
    doc.step.tcp_ping
    doc.step.win_cmd


Step Types
----------

There are a number of different types of steps in Attune. The different types
of steps have
different attributes which will be displayed in the right hand pane when a
step is created or edited.


There are a number of different types of steps in Attune

:Deploy Archive: send Archive to a specified path on a Server

:Deploy RPM Archive: send .rpm archive to specified path on a Server

:Deploy Template Archive: send Template Archive to specified path on a Server

:Interactive Shell step: execute Shell step that requires user interaction

:Execute Shell Step: execute procedure written in Bash, Python or Perl

:Execute SQL Step: execute SQL or PL/SQL procedure on a Server

:Execute Windows Script: runs a Windows Batch file, Powershell script, or script
                for specified
                (custom) interpreter on a Windows Server

:Group Step: a procedure made up of other procedures or steps

:Setup SSH keys: sets up SSH key pair between Attune and designated
                Server for secure login
