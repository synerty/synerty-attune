.. _attune_help_step_tcp_ping:

========
Tcp Ping
========

The Tcp Ping step checks to see if a TCP socket accepts connections on a remote
    server.

This is not an ICMP ping. The first intended use for this step was to check if a server
    had completed rebooting.

Options:

:Tcp Port: The TCP Port to attempt connecting to.

:Pre Wait Time: The time to wait before starting the TCP Ping (Seconds)

:Minimum Downtime: The minimum time that the TCP port must fail accepting connections for (Seconds)

:Post Wait Time: The time to wait before starting the TCP Ping (Seconds)

