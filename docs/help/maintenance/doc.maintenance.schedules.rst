.. _attune_help_maintenance_schedules:

=========
Schedules
=========

Attunes Schedules allow you to set Plans to run automatically at a designated date
and time, or a series of different
dates and times.


They allow you to set Plans to run at regular designated intervals starting at a
particular date and time.
You will need to have the Plans defined in the Plans screen first.


.. image:: doc.maintenance.schedules.png

:1: List of defined schedules with search box to find a
                schedule in the list


:2: Add Button - to create new schedule

:3: Button to enable or disable the Attune scheduler on or
                off.
                The button is green when the scheduler is active.
                The button is red when the scheduler is turned off


:4: Name of Schedule
                Each schedule needs a unique name. The schedule name will be different to
                the name of
                the Plan to be executed in the schedule.


:5: Number of jobs to be kept
                Specifies the number of the most recent Jobs of the Plans in the
                current schedule to be kept with their respective
                Plans in the Jobs section.
                When the scheduler runs the Plans specified in the schedule a Job
                will be generated for
                each Plan, each time it is run. In the jobs screen, each of these
                Plans is listed in the
                left hand pane. If a Plan has more than one saved Job, there will be
                a drop down button on
                the right. Click on this button to display the list of jobs, with
                their date and time of
                job shown. Clicking on one of these jobs will show details of
                which steps of the procedure
                executed properly, and details of any procedure steps that failed.


:6: Enabled button - shows whether currently selected
                schedule is enabled or disabled. Scheduled
                events can be set up and disabled when not required. They can then be
                enabled again later.


:7: Comments
                You can add descriptions, explanations and notes related to the current
                schedule


:8: Date and time for job.
                Specify dates and times for Plans to execute. You can get a plan to repeat
                on a regular basis by
                checking the repeat box, and selecting a time internal for the plan to
                repeat. You can also add
                a number of other dates and times for the plan to execute - these can also
                be repeating if required.
                Each of these planned starts has an 'Enabled' button on the left hand
                side. They are enabled by
                default when you create them. You can disable a particular schedule by
                clicking the button
                and setting job at that particular start date to 'Disabled'. There
                is also a button on the
                right hand side that will delete a particular scheduled start date when
                clicked.


:9: The Plan, or Plans, to be executed. These will need to
                be defined in the Plans section - where you
                specify the Procedure to be executed, and assign values to any
                variables used in the procedure.
                Select a Plan from the drop-down list. You can then select other Plans to
                run, with an optional delay period
                between them. This allows you to run a sequence of Plans. You can delete a
                Plan from the sequence by clicking
                on the 'x' button at the right of the Plan you wish to delete from the
                schedule.


:10: Actions to be taken when schedule has run
                You can set up a schedule so it sends emails to designated email
                addresses, depending on the
                the outcome produced by the job of the plan in the schedule. For
                example, you can set a schedule to
                email one email address if the schedule runs successfully, or email
                different address if it fails.


:11: Save Reset Delete Buttons
                Click the Save button to save the schedule details you have added. The
                'Reset' button will clear all
                the details you have added and set all fields back to their default
                values. The 'Delete' button will
                delete the currently selected schedule.

