.. _attune_help_maintenance:

===========
Maintenance
===========

In the top menu of Attune, there is a drop down menu marked 'Maintenance'. This
allows you to access a number of different
features of Attune


.. toctree::
    :maxdepth: 2

    doc.maintenance.steps
    doc.maintenance.schedules
    settings/doc.maintenance.settings
    doc.maintenance.license


.. image:: doc.maintenance.intro-screen.png

:Steps: Manage procedure steps and view dependency relationships between steps.

:Schedules: Schedules

:Settings: Settings

:License and Updates: License and Updates

