.. _attune_help_maintenance_settings:

========
Settings
========

The settings screen allows you to view and alter some of the server settings.

Server Settings
---------------

Server Settings

You can alter the settings if required. Click on the 'Save' button at the bottom
to save your changes.

----

.. image:: server-settings.png

----

:Server Name: The descriptive name of this Attune server.

:Server Description: The description of this Attune server.

:Enail SMTP Server: The SMTP Smart Host server where this Attune server
    will send it's email to.

:Server Admin Email Address: The email address of the administrator to receive all
    Attune administration emails.

:Email Sender: The email address that emails will be from when sent from this
    Attune server.

LDAP Settings
-------------

LDAP connection is an **Enterprise** feature.

Attunes LDAP works seamlessly against the Microsoft Active Directory
Lightweight Directory Service (AD LDS).

----

The following is a good article that describes how to enable LDAP over SSL (LDAPS)
on Windows 2012.

`<https://social.technet.microsoft.com/wiki/contents/articles/2980.ldap-over-ssl-ldaps-certificate.aspx>`_


----

To configure LDAP:

#.  Select the **LDAP Settings** from the settings dropdown box,

#.  Set the LDAP settings, including "LDAP Enabled"

#.  Click Save

Now attempt to login with an LDAP user.

----

.. image:: ldap-settings.png

----

:LDAP Enabled: Should Attune attempt LDAP authentication at all.

:LDAP Domain Name: The domain name of the LDAP installation,
    for example :code:`domain.example.org` where domain.example.org is the name of your
    active directory domain.

:LDAP URI: The connection string to the LDAP server, example values are:

    *  :code:`ldap://server1.example.org`

    *  :code:`ldap://domain.example.org`

    *  :code:`ldaps://10.2.2.2`

:LDAP CN Folders: This is a comma separated lost of CN paths to search during login

:LDAP OU Folders: TThis is a comma separated lost of OU paths to search during login

:LDAP Attune Admin Group: The LDAP group name for the Attune Admin user role.
    Users in this group will be able to perform all tasks in Attune.

    Default value : :code:`attune-admin`

:LDAP Attune Job Group: The LDAP group name for the Attune Job user role.
    Users in this group will be able to:

    * Create, edit and delete plans.
    * Create, run and delete Jobs.

    They will not be able to edit procedures, variables, values,
    archives or server settings.

    Default value : :code:`attune-job`

:LDAP Attune Dashboard Group: The LDAP group name for the Attune Dashboard user role.
    Users in this group will only be able to:

    * View the Attune Dashboard
    * Run or stop jobs from the Attune Dashboard

    Default value : :code:`attune-dashboard`
