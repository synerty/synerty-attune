.. _attune_help_maintenance_license:

==================
License and Update
==================

The license and update option listed in the Maintenance menu is where you can renew the license of Attune, and apply
new updates to Attune.  When you choose this option the screen appears as shown

.. image:: doc.maintenance.screen1.png

Before you update Attune you should make a backup of the Attune server.  Updates of Attune are distributed as a
compressed Archive file.  At the top of the Maintenance screen is the place where you can apply updates to Attune.
When you receive an update file, drag and drop it into the yellow box marked 'Drop Attune release here'

To update the license for Attune you will need to note the Server ID for the license as shown on the screen.  A new
license key can be obtained by sending an email to support@servertribe.com
with the subject line 'Attune'=<em>Server ID

When you receive a new license key, copy and paste it into the 'Update license' box at the bottom of the screen,
then click the 'Update' button at the bottom of the screen.  This will update your Attune license.






