.. _attune_help_maintenance_steps:

=====
Steps
=====

The Steps screen shows all the available procedure steps that you currently have
available to run in Attune, and shows
the dependency relationship between these steps. You cannot create a step here -
that is done in the Procedures screen.
You can modify procedures here, as well as delete them. You cannot delete
procedures in the Procedures screen - this
must be done here

.. image:: doc.maintenance.steps.png



:1: List of steps with the Currently selected step
                highlighted.
                The two numbers in the bottom right hand corner show how many dependencies
                there are between the
                currently selected step and other steps. The number on the left shows how
                many other steps reference the
                currently selected step - and depend on it for their operation. The number
                on the right shows how many other
                steps the currently selected step references or makes use of - and is
                dependent upon.


:2: Links section - steps which have a dependency
                relationship with the currently selected step.
                For each linked procedure it shows -

                    *   Procedure name
                    *   Unlink button - to unlink the named procedure from currently
                        selected procedure

                    *   Goto button - goes to named procedure so you can view its
                        details




:3: Steps which make use of, and depend on, the currently
                selected step. If the currently selected
                step is unlinked or deleted, these steps will be impacted


:4: Steps which the currently selected step make use of and
                depend on. If the
                the currently selected step is unlinked or deleted, these steps will not
                be impacted. If these steps are
                unlinked or deleted, the currently selected step will be impacted.



:5: Details Section - shows details of currently selected
                step. Details
                will vary depending on the type of procedure. Group steps will show the
                step name and comment. Job
                steps will show more details - including the program code of the step. The
                code for a job step can
                be edited wth Attune's built in editor. Job steps have a 0 as their
                right hand number.


:6: Save and Reset buttons - save changes made to details,
                or reset changes
                made since last save.


See :ref:`attune_help_steps`
