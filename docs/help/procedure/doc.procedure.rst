.. _attune_help_procedures:

==========
Procedures
==========

Attune's Procedures allow to you to perform operations and execute commands across
a number of different
machines, running under Windows and Linux operating systems. Scripts written in
Bash, Perl, Python
PHP, and Windows Powershell can all be run on a number of different machines while
being still being held
centrally in Attune for consistency and ease of maintenance.

Attune's Procedures also make use of Variables in scripts, which can have
different values mapped to
them prior to job. This facilitates the creation of procedures that are
generic and re-usable.
You can deploy the same script to across different machines to achieve different
results by mapping
different values to the variables in the procedures prior to job.


.. image:: doc.procedure_screen1a.png

The left pane shows a list of procedures that have already been created.
There are buttons at the top to -

*   Create a New Procoedure
*   Import a Procedure into Attune from a file (.atp file)

The middle pane shows the current selected procedure, and the procedures that are
contained in it.
Group steps have a '+' to their left. Click to expand and view procedures
contained in group step
There are buttons at the top to -

*   Add a new procedure into the currently selected procedure
*   Link an existing procedure into the currently selected procedure as a step

*   Export a Procedure to an .atp file

The right hand pane shows details of the current procedure or sub-procedure
selected.

*   details shown will vary depending on the type of procedure selected
*   procedures that execute programs or scripts will have an editor window at the
    bottom,
    allowing you to view and edit the file.


Creating a Procedure
--------------------

To create a Procedure, click on the 'Create' button at the top left of the screen,
and the following
dialog will appear on the right. Enter a name in the 'Step Name' field to identify
your new procedure.
You can add more descriptive text in the 'Comments' box at the bottom.

There is a checkbox called 'IsProcedure' which is checked by default - indicating
you are creating a
'Group Step' procedure. If you uncheck this box, a 'type' box appears at the top
of the dialog,
allowing you to specify the type of procedure you wish to create.

.. image:: doc.procedure_create1.png

If you select a different type of procedure the screen will change to allow you to
add details specifically
required by the different procedure types.

Some of the procedures - the ones that execute scripts or commands, feature a
script area where your scripts
or programs can be created and edited with Attune's built in text editor.

When you have added the required information in the spaces provided, click on the
'Create' button at the
bottom of the screen to save your procedure.

When you create a new procedure the default type is 'Group Step' because most
procedures are made up of
a series of other procedures, or groups of procedures, which you can create, or
add to a procedure by
using the 'Add' and 'Link' Buttons at the top of the screen.

Editing a Procedure
-------------------

Select a procedure to edit from the list in the left hand pane. The middle pane
will show the structure
of the procedure, and the Group steps and procedures that are contained in the
procedure.

.. image:: doc.procedure_midpane.png


If you click on any of the Procedure steps, the details of that procedure will be
shown in the right
hand pane for editing.

At the bottom of the right hand pane there are three buttons

*   Save - save changes you have made to the procedure
*   Reset - undo any changes made since last save
*   Unlink - remove a procedure out of the currently selected procedure.
    Procedure will not be deleted, just taken out of current procedure.

If you forget to click on save before moving to another procedure, your changes
will be lost. The buttons
are at the bottom of the screen, and you will often have to scroll down to see
them.


When you edit a procedure that is a step or a group step inside a main procedure,
there will be two fields at the top
of the right hand pane - link name and link order


If you specify a link name for the procedure step, the procedure name will be
renamed as **linkname(procedure name)**
in the tree hierarchy diagram. This allows you to give more meaningful and
descriptive names to the procedure steps, especially
where you are using a procedure step a number of times in a procedure. You can
give a procedure step different link names when
you use it in different parts of your procedure so you can easily differentiate
between the different instances of the procedure
step.


The link name does not group procedure steps together, or affect their order of
job. You can remove the link name
from a procedure step and the procedure name will appear without the link name,
but will otherwise not be affected by the
removal of the link name.


Link order is a number that specifies the order in which steps in a procedure are
executed. When you create a step
in a procedure, or link in a pre-defined procedure as a step, the step is given a
default link order value indicating
its position in the procedure. If you alter this link order number so that it is
less than the link order number of
the preceding step, the position of the two steps and their job order will
be reversed.


This allows you to change the job order of steps within a procedure if
required.

Deleting a Procedure
--------------------

You cannot delete a procedure from the 'procedures' screen in Attune - you will
need to use Attune's step
maintenance function. This provides better facilities for showing how procedures
are linked together, and
dependency relationships between procedures.


Deleting a procedure that is used as a step in another procedure will cause
problems.
To delete a procedure, you will first need to unlink it from all other procedures.


Click on 'Maintenance' on the menu along the top of the screen, and select 'Steps'
from the drop down menu.
The Steps screen will appear


If you select one of the procedures from the list on the left, the details of that
step will be
displayed on the right.


If you click on the 'unlink' button, a dialog box will appear asking you to
confirm that you wish
to unlink the procedures. If you select 'ok' the procedure will be unlinked and
this will be permanent -
you do no need to click the 'Save' button at the bottom of the screen. If you want
to restore the
link you will need to go to the 'Procedures' section to manually link the
procedures again.



If you have unlinked the current procedure from all other procedures, a red
'Delete' button
will appear at the bottom next to the 'Save' and 'Reset' buttons. If you click on
this button the
current procedure will be deleted. You can unlink the current procedure from all
other procedures but
still keep it as a saved procedure if you wish.



Importing a Procedure
---------------------


Procedures can be imported into Attune. If you have an Attune procedure file -
with a .atp extension
you can import the procedure into Attune by clicking on the 'Import' button at the
top left of the screen.
A dialog will appear with a highlighted area where you can drag and drop your
procedure file into


.. image:: doc.procedure_import.png


Exporting a Procedure
---------------------

Attune procedures can be exported to a file, which will have a .atp extension. You
can also include
values used by your procedure, and archives if your procedure has any 'Deploy
Archive' steps.
Select the procedure you wish to export from the list on the left hand side, and
click on the
'Export' Button.


A Dialog will appear


.. image:: doc.procedure_export1.png


If there are any archives specified in any 'Deploy Archive' procedures, they will
be listed. Click to
check any Archives you want to include with your exported procedure.



If you click on the Values tab you will see a list of values that are
currently mapped,
in a Plan, to the variables used in your procedure. Click to check any
Values you want to
include with your exported procedure.





