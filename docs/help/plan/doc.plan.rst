.. _attune_help_plans:

=====
Plans
=====

Plans allow you to assign Values to the Variables that appear in a
Procedure prior to job.
You can create a number of different Plans for the same procedure and assign
different values to the variables in each plan.


When you access the Variables screen from Attune's navigation panel at the top
of the screen,
you will be presented with a screen with two panels as shown below.



.. image:: doc.plan.screen.png

:1: The left hand panel shows a list of the plans that have
                already been created

:2: A search box that allows you to search for a plan that
                was previously created

:3: Add button that allows you to create a new plan

:4: View and edit the attributes of the plan, and assign
                values to variables


Creating a Plan
---------------

If the right hand pane has 'Create Plan' at the top, with blank fields, you can
create a new plan by entering a name,
and selecting the procedure you wish to run from the drop-down box provided. You
can also add a comment, and
specify whether the Plan will show in the Attune Dashboard or not. Click the
'Create' button at the bottom when
complete


You can also click on the +Add button in the top right hand side of the left pane,
next to the search box.
This will display the 'Create Plan' dialog in the right hand pane.


After you click the 'Create' button at the bottom of the screen, the Plan will be
created. The screen will
then change to the 'Edit Plan' screen and display a list of variables that
appear in the procedure.
You can now supply values for the Variables or select values from drop down
boxes,depending on the type of
variable. If any variables have been defined with default values, they will
be displayed and can be
altered if required. All the Variables will need to be assigned values prior to
job, or this will
cause an error.


.. image:: doc.plan.screen2.png


If you change to 'Show in Dashboard' option to Yes, the plan will be displayed on
the Attune dashboard, and can
be run directly from there.


.. image:: doc.plan.dashboard_scr1.png


Click on the 'Play' button on the right hand side of the plan you wish to execute

.. image:: doc.plan.dashboard_scr2.png


Plans do not have to be added to the Attune dashboard. All Plans can be run by
creating a Job and linking
it to the Plan. Jobs provide more flexibility and more extensive feedback
when plans are executed.


Editing a Plan
--------------


Select one of the Plans listed in the left hand pane by left clicking it. The
right hand pane will show a list of
the attributes of the Plan, and a list of the Variables and the Values
that appear in the Plan. The
attribute values can be altered, and different Values can be assigned to the
Variables. The Variables
shown depend on the Variables used in the Procedure linked to the Plan - these
can only be altered by altering
the Procedure.


When complete, click the 'Save' button at the bottom of the dialog box.


.. image:: doc.plan.screen3.png

Deleting Plan
-------------

Select one of the Plans listed in the left hand pane by left clicking it. The
details will be shown in the right
hand pane. Then simply click on the 'Delete' button at the bottom of the dialog
box.

Downloading Plan Documentation
------------------------------

When a Plan is created, Attune will generate documentation for the plan. A link
will appear in the top right
corner of the Edit Plan screen. Click on the link to download the documentation in
Word format.





