.. _attune_help_values:

======
Values
======


In Attune, the Procedures you create are executed under a Plan. The Plan is where
you assign different Values
to the Variables that appear in the Procedures. The Plan is then run in an
Job.


All Variables that appear in the steps of a Procedure must be assigned a
Value in the Plan before it can
be run in a Job - otherwise an error message will be generated.


Values must be created in Attune so they can be assigned to Variables in a
Plan.


When you access the Variables screen from Attune's navigation panel at the top
of the screen,
you will be presented with a screen with two panels as shown below.



.. image:: doc.value_add2.png

:1: The left hand panel shows a list of the values that
                have already been defined,
                along with their types

:2: At the top of the left panel there is a search box that
                allows you to search for a particular
                value

:3: Next to the search box there is an Add button that
                allows you to create a new
                value

:4: The right hand panel is where you can view and edit the
                attributes of the
                values


Types of Value
--------------


Each value has a type which conforms to one of the value types defined
in Attune. Values are
assigned a type when they are created and this will determine what other
attributes the value has.
The different types of value in Attune are -


:OS Credential: Supply login details for OS
                will have additional attributes: Name, userid, password, comments

:Server: Allows access to Server details -
                will have additional attributes: Name, IP Address, Hostname, OS Type,
                Domain Name, Comment

:Server Group: Allows you to access servers within a defined server
                group
                will have additional attributes: Name, List of Servers in Group, Comment

:SQL Credential: Supply login details for SQL databases
                will have additional attributes: Name, SID, userid, password, Sysdba
                checkbox, Comment

:IPv4 Subnet: Define a network Subnet
                This variable provides a succinct definition of the details of a IPv4
                network subnet.

:Text: Supply a pre-defined text value
                will have additional attributues: Name, Value, Comment


Creating a Value
----------------


If the right hand pane has 'New Value' in the titlebar, with blank fields,
you can create a new value
by adding the required details into the fields provided. Alternatively you can
click on the +Add button in the
top right hand side of the left pane, next to the search box. This will display
the 'New Value' dialog in the
right hand pane.



.. image:: doc.value_new.png


The first attribute you must specify is the Type of value to be created.
There is a drop down box for
selecting one of the value types defined in Attune. When this has been
selected the form will change and
display the remaining attributes that can be added or changed - depending on the
Value type selected.

For example, if you select 'Server" as the value type, the right hand pane
will change to show the attributes
shown below


.. image:: doc.value_newserver.png



You then add the attributes in the spaces provided, then click on the 'Create'
button at the bottom.


Editing a Value
---------------



Select one of the Placevlaues listed in the left hand pane by left clicking it.
The fields will be shown in the right
hand pane. The fields can then be altered as required. When complete, click the
'Save' button at the bottom of the
dialog box.


.. image:: doc.value_edit.png


Deleting Value
--------------

Select one of the Values listed in the left hand pane by left clicking it.
The fields will be shown in the right
hand pane. Then simply click on the 'Delete' button at the bottom of the dialog
box.

Referencing Value attributes in Scripts
---------------------------------------

Values are assigned to a variable prior to job. The value
attributes are referenced in scripts
by using the variable name and the value attribute separated by a dot and
enclosed in curly braces.


:code:`{placeHolderName.placeValueAttribute}`

Note that the variable attribute names are slightly different from the ones
displayed on the screen where you
create and edit the values. The variable name is also modified slightly -
stripped of any separating
with only the first letter of the second and any subsequent words capitalised.

More details about using variables and values in scripts, including a list
of the value attributes,
are available

:ref:`attune_help_values_in_scripts`

