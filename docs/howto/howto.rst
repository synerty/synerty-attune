.. howto_:

======
How To
======

.. toctree::
    :maxdepth: 2
    :caption: Contents:

    setup_winrm_cifs_manually/setup_winrm_cifs_manually
    setup_winrm_via_ad/setup_winrm_via_ad
