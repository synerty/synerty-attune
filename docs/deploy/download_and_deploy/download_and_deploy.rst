.. _download_and_deploy:

==========================
Downloading And Deployment
==========================

Attune is distributed as a Virtual Appliance.

Download the Attune VA template and deploy it to VMWare, VirtualBox or infrastructure of your choice.


Step 1: Purchase your desired Attune License and fill out the billing details. in this example we have purchased the Starter License.

Step 2: Click and download both links on the “Purchase successful” page. if your purchase was not successful, contact us here. (please insert link to support)

.. image:: deploy_license_file_links.png

Step 3: Deploy Attune to your Virtual Machine Software. instructions on how to do this using VirtualBox can be found here.

Step 4: enter the Attune login code: the username is “root” and the password is “ServerTribe”

.. image:: deploy_login_as_root.png

Step 5: type into the VM terminal:
ifconfig | grep cast

.. image:: deploy_run_ifconfig.png

Step 6: insert your Inet address into your browser.

.. image:: deploy_enter_address_bar.png

Step 7: enter the username and password: the default username is admin and the default password is integrate=9

Step 9: click Maintenance, then click license and Updates.

Step 10: now copy your server ID.

.. image:: deploy_goto_license_screen.png

Step 11: go back to server tribe, and click purchase history.

Step 12: on your desired purchase, click “get license”

Step 13: enter your Server ID into the box.

Step 14: Collect your License Key.

Step 15: go back to Attune and paste the license Key here:

.. image:: deploy_license.png


Enjoy Attune!
