.. _deploy_to_virtualbox:

====================
Deploy to VirtualBox
====================

Attune is distributed as a Virtual Appliance.

Download the Attune VA template and deploy it to VMWare, VirtualBox or infrastructure of your choice.

This Support guide is tailored for osX, deploying to VirtualBox.


Step 1: open virtualBox and, in the top left, click file. afterwards, click “import appliance”

.. image:: virtualbox_import.png

Step 2: direct the VirtualBox importer to the first downloaded item.

Step 3: set the Operating System to linux Debian (64bit) and click Import.

.. image:: virtualbox_select_ova.png

Step 4: click on the new virtual machine and click “settings”

.. image:: virtualbox_configure.png

Step 5: set the network adapter to “bridged adapter”

.. image:: virtualbox_settings.png

Step 6: you have successfully deployed the virtual machine to VirtualBox,
proceed to continue with the Attune activation Process, which can be found here :
:ref:`download_and_deploy`.

.. image:: virtualbox_network.png
