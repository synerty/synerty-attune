.. _install_attune:

==============
Install Attune
==============

This section describes how to install attune in to a prepared Linux environment.

Sudo
----

Setup sudo access for Attune, Run this step as :code:`root`. ::

        echo "attune        ALL=(ALL)       NOPASSWD: ALL" > /etc/sudoers.d/attune

Database
--------

.. note:: All these steps must be run as the :code:`attune` user

Create the attune SQL user: ::

        sudo su - postgres -c "createuser -d -r -s attune"

----

Create the database: ::

        createdb -O attune attune

----

Set the PostGreSQL attune users password: ::

        psql <<EOF
        \password
        \q
        EOF

        # Set the password as "PASSWORD" for development machines
        # Set it to a secure password from https://xkpasswd.net/s/ for production


----

Cleanup traces of the password: ::

        [ ! -e ~/.psql_history ] || rm ~/.psql_history

Bash Profile
------------

Setup the profile for Attune, Edit :file:`~/.profile` and append the following lines ::

        cat <<"EOF" >> ~/.bash_profile
        export PATH=~/python/bin:$PATH
        EOF

----

Close the shell ::

        exit

----

Re-open the shell

Config
------

Create the Attune config file ::

        [ -d ~/attune.home ] || mkdir ~/attune.home
        cat <<"EOF" >> ~/attune.home/config.cfg

        [disk_storage]
        home_path = /home/attune/attune.home
        archives_path = %(home_path)s/archives_contents
        tmp_path = %(home_path)s/tmp
        archives_unlocked_contents = %(home_path)s/archives_unlocked_contents


        [alembic]
        # path to migration scripts
        script_location = /home/attune/python/lib/python2.7/site-packages/attune/alembic
        sourceless = true
        sqlalchemy.url = postgresql://attune:PASSWORD@localhost/attune

        EOF

.. note:: Replace the PASSWORD with the password set for the postgresql attune user

Set Git User
------------

Run the following command to setup the commit user for git. ::

    git config --global user.email "attune@attune.local"
    git config --global user.name "Attune"

Deploy Attune
-------------

This section sets up the python virtual environment for Attune and deploys the Attune
packages.

----

Stop Attune if it's running: ::

        sudo systemctl stop attune

        pkill -9 -f run_attune_server

----

Remove existing releases from the server: ::

        rm -rf ~/synerty-attune-v*

----

Remove the existing attune virtual environments if required: ::

        rm -rf ~/python

----

Create a new python virtual environment where all of the Attune packages
will be installed. Run the following command: ::

        virtualenv ~/python

----

Make sure the python virtualenv is in the :code:`PATH` ::

        which python
        [ '/home/attune/python/bin/python' == `which python` ] && echo 'Success' || echo 'FAILED!!!'

----

Copy the Attune release to the server, to the :code:`attune` users home directory,
as the user :code:`attune`.

For example: :file:`synerty-attune-v3.0.0-release.tar.bz2`

----

Install the latest Attune: ::

        cd

        tar xvjf synerty-attune-v*.tar.bz2
        pushd synerty-attune-v*-release

        # Deploy the software
        pip install --upgrade --no-cache --no-index --find-links=py py/attune-*.tar.gz

        # Install the init script
        sudo cp init/attune.service /lib/systemd/system/

        # Return to the last dir
        popd

----

Try running Attune manually: ::

        which run_attune_server
        run_attune_server

----

Kill the server with :code:`Control + C`

----

Finally, enable the Attune service and Auto Start: ::

        sudo systemctl enable attune
        sudo systemctl start attune

----

Attune setup is complete.
