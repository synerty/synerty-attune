.. _setup_os_requirements_rhel:

==========================
Setup OS Requirements RHEL
==========================

This section describes how to perform the setup for Red Hat Linux Server 7.4.  The Attune platform
is designed to run on Linux.

Please read through all of the documentation before commencing the installation procedure.

Installation Objective
----------------------

This Installation Guide contains specific Red Hat Linux Server 7.8 operating system requirements
for the configuring of synerty-attune.

Required Software
`````````````````

Some of the software to be installed requires internet access. For offline installation
some steps are required to be installed on another online server for the files to be
packaged and transferred to the offline server.

Below is a list of all the required software:


*   Python 2.7.x

*   Postgres 9.x.x

Suggested Software
``````````````````

The following utilities are often useful.

*   rsync

*   git

*   unzip


Installation Guide
------------------

Follow the remaining section in this document to prepare your RHEL operating system for
to run the Attune Platform.

The instructions on this page don't install the attune platform, that's done later.

Install Red Hat Linux Server 7.x OS
-----------------------------------

This section installs the Red Hat Linux Server 7.x 64bit operating system.

Create VM
`````````

Create a new virtual machine with the following specifications

*   2 CPUs
*   2gb of ram
*   100gb of disk space

Install OS
``````````

Download the RHEL ISO **Red Hat Enterprise Linux 7.x Binary DVD** from:

`Download RHEL <https://access.redhat.com/downloads/content/69/ver=/rhel---7/7.x/x86_64/product-software>`_

----

Mount the ISO in the virtual machine and start the virtual machine.

.. note::

    Run through the installer manually, do not let your virtual machine software perform
    a wizard or express install.

Starting Off
~~~~~~~~~~~~

At the **Red Hat Enterprise Linux 7.x installer boot menu** screen, select: ::

    Install Red Hat Enterprise Linux 7.x

----

At the language selection screen, select: ::

    English

----

Next you will see a screen that lets you jump to any area to configure.
The areas that need attention are numbered and explained in the following sections.

.. image:: config_menu1.png

.. image:: config_menu2.png

----

#1 Goto the  **DATE & TIME** screen, select the appropriate time location.

.. image:: rhel_date_and_time.jpg

----

#2 Goto the **SOFTWARE SELECTION** screen, select **Minimal Install**
or **Server with GUI** if you'd like a GUI.

.. image:: rhel_software_selection.jpg

----

#3 Goto the **INSTALLATION DESTINATION** screen

The following partitioning is recommended for DEV attune virtual machines.

Select: ::

    I will configure partitioning.

.. image:: rhel_installation_destination.jpg

Select Done.

Partition Table
~~~~~~~~~~~~~~~

We'll be creating three partitions, `/boot`, `/` and `swap`. For a heavily used production
server you may want to create more virtual disks and separate out `/var`, `/home`, and `/tmp`.
With one file system per disk.

Having one file system per disk allows VM
software to easily expand the disk and filesystem as required.

----

Select **Standard Partition**

Again, This is to allow the virtual machine software to expand the DEV server
disks more easily.

.. image:: rhel_standard_partitioning.png

----

Add the partitions, for each partition, click the plus.

.. image:: rhel_new_partition.png

----

Set the Mount Point to **/boot**

Set the size to **1g**

Click **Add mount point**

.. image:: rhel_new_mount_boot.png

----

Set the Mount Point to **swap**

Set the size to **8g**

Click **Add mount point**

.. image:: rhel_new_mount_swap.png

----

Set the Mount Point to **/**

Set the size to **100%**

Click **Add mount point**

.. image:: rhel_new_mount_root.png

----

You should have a partition layout as follows, Click **Done**


.. image:: rhel_example_partition.png

----

Click **Accept Changes**


.. image:: rhel_confrm_partition.png

----

#4 Goto **NETWORK & HOST NAME** screen,

.. image:: rhel_network_hostname.jpg

1. Enter your desired hostname, for example ::

    attune.localdomain

----

2. Turn on the Ethernet connection, this will get a DHCP IP Address.

.. note:: Make note of the DHCP IP Address

Otherwise, Configure a static IP address,

    a. Goto IPv4 Settings tab,

    b. Set Method to `Manual`,

    c. Add static IP address,

    d. Save.

    .. image:: rhel_network_static_ip.jpg

----

Select **DONE** review the **SUMMARY OF CHANGES**

----

Click **BEGIN INSTALLATION**

.. image:: rhel_begin_installation.png

----

While RHEL is installing, further installation steps need to be completed.

Configure the **ROOT PASSWORD** and the **USER CREATION**

.. image:: rhel_installing_root_user_config.png

----

Configure the root password of the new RHEL VM.

Enter the root password twice, then click **Done**

.. image:: rhel_config_root_pass.png

----

Create the **attune** user as follows.

.. image:: rhel_create_user.png

----

Click **Finish configuration**

.. image:: rhel_finish_configuration.png

----

Click **Reboot**

.. image:: rhel_install_reboot.png

----

After the server has rebooted, disconnect and remove the RHEL ISO
from DVD drive in the VM software.


----

The OS installation is now complete.

Login as Attune
---------------

Login to the RHEL VM as the :code:`attune` user, either via SSH, or the graphical desktop if it's
installed.

.. important:: All steps after this point assume you're logged in as the attune user.

Registering RHEL
----------------

The RHEL server must have access to the redhat repositories at rhn.redhat.com to install
the required packages.

This section describes one way of registering a new RHEL server to a Redhat subscription.
This is a paid subscription.

----

Run the following command to register the system.
Replace MY_RHN_USERNAME with your redhat network username. ::

    sudo date
    # enter the password for attune

    sudo subscription-manager register --username MY_RHN_USERNAME
    # Enter the password for the RHN account

----

List the subscriptions, and select a pool.
 ::

    sudo subscription-manager list --available | grep Pool


Subscribe to the pool.
Replace POOL_ID_FROM_ABOVE_COMMAND with the Pool ID from the last command. ::

    sudo subscription-manager subscribe --pool=POOL_ID_FROM_ABOVE_COMMAND

----

Test the subscription with a yum update, this will apply the latest updates.
 ::

    sudo yum update -y

.. note::

    If you want to remove the server from the pool, and unregister it, run the following.

    ::

        sudo subscription-manager remove --all
        sudo subscription-manager unregister

Configuring the OS
------------------

Removing IPv6 Localhost
```````````````````````

Run the following command to ensure that :code:`localhost` does not resolve
to :code:`::1` as this effects the PostGreSQL connection. ::

    sudo sed -i '/::1/d' /etc/hosts


Installing OS Prerequisites
---------------------------

This section installs the OS packages required.

.. note:: Run the commands in this step as the `attune` user.

----

To begin, make sure that all the packages currently installed on your RHEL
system are updated to their latest versions: ::

        sudo yum update -y

----

Install the C Compiler package, used for compiling python or VMWare tools, etc: ::

        PKG="gcc gcc-c++ kernel-devel make"
        sudo yum install -y $PKG

----

Install utilities: ::

        PKG="rsync"
        PKG="$PKG unzip"
        PKG="$PKG wget"
        PKG="$PKG bzip2"
        PKG="$PKG net-tools"

        sudo yum install -y $PKG

----

Install the Python build dependencies: ::

        PKG="curl git m4 ruby texinfo bzip2-devel libcurl-devel"
        PKG="$PKG expat-devel ncurses-libs zlib-devel gmp-devel"
        PKG="$PKG openssl openssl-devel"
        sudo yum install -y $PKG


----

Install C libraries that some python packages link to when they install: ::

        # For the cryptography package
        PKG="libffi-devel"

        sudo yum install -y $PKG


----

Install C libraries for the lxml: ::

        PKG="libxml2 libxml2-devel"
        PKG="$PKG libxslt libxslt-devel"

        sudo yum install -y $PKG


----

Install C libraries that the oracle client requires: ::

        PKG="libaio libaio-devel"

        sudo yum install -y $PKG


----

Install C libraries required for LDAP: ::

        PKG="openldap-devel"

        sudo yum install -y $PKG


----

Install C libraries required for Pillow / Imaging: ::

        PKG="libjpeg-turbo-devel"
        PKG="$PKG zlib-devel"
        PKG="$PKG libtiff-devel"
        PKG="$PKG libwebp-devel"
        PKG="$PKG openjpeg-devel"
        PKG="$PKG libtiff-devel"
        PKG="$PKG freetype-devel"
        PKG="$PKG lcms2-devel"

        sudo yum install -y $PKG


----

Install python packages needed for the virtual python environment: ::

        PKG="python-virtualenv"
        PKG="$PKG python-wheel"

        sudo yum install -y $PKG


----

Cleanup the downloaded packages: ::

    sudo yum clean all


Installing VMWare Tools (Optional)
----------------------------------

This section installs VMWare tools. The compiler tools have been installed from the section
above.

----

In the VMWare software, find the option to install VMWare tools.

----

Mount and unzip the tools: ::

    sudo rm -rf /tmp/vmware-*
    sudo mount /dev/sr0 /mnt
    sudo tar -xzf /mnt/VM*gz -C /tmp
    sudo umount /mnt


----

Install the tools with the default options: ::

    cd /tmp/vmware-tools-distrib
    sudo ./vmware-install.pl -f -d


----

Cleanup the tools install: ::

    sudo rm -rf /tmp/vmware-*


----

Reboot the virtual machine: ::

    sudo shutdown -r now


.. note:: Keep in mind, that if the static IP is not set, the IP address of the VM may
    change, causing issues when reconnecting with SSH.


Install PostGreSQL
------------------

Install the relational database Attune stores its data in.
This is PostGreSQL 10.

.. note:: Run the commands in this step as the `attune` user.

----

Install PostGreSQL: ::

    PKG="postgresql"
    PKG="$PKG postgresql-server"
    PKG="$PKG postgresql-contrib"
    PKG="$PKG postgresql-devel"
    PKG="$PKG postgresql-libs"
    sudo yum install -y $PKG

----

Create the PostGreSQL cluster: ::

    sudo postgresql-setup initdb

----

Allow the attune OS user to login to the database as user attune with no password ::

    F="/var/lib/pgsql/data/pg_hba.conf"
    if ! sudo grep -q 'attune' $F; then
        echo "host    attune    attune    127.0.0.1/32    trust" | sudo tee $F -a
        sudo sed -i 's,127.0.0.1/32            ident,127.0.0.1/32            md5,g' $F
        sudo sed -i 's,::1/128                 ident,::1/128                 md5,g' $F
    fi

----

Configure PostGreSQL to auto start: ::

    sudo systemctl enable postgresql
    sudo systemctl start postgresql


Setup vCenter API Support
-------------------------

Powershell v6.x.x can be downloaded from github
 `https://github.com/PowerShell/PowerShell/releases`

---

Install the Powershell RPM ::

    sudo yum install 'https://github.com/PowerShell/PowerShell/releases/download/v6.2.4/powershell-6.2.4-1.rhel.7.x86_64.rpm'

---

Install the Powershell VMWare.PowerCLI module ::

    sudo pwsh -Command "Install-Module VMware.PowerCLI"


.. note:: You can download this module for an offline install with
    the following command ::

        sudo pwsh -Command "Save-Module -name VMware.PowerCLI -Path /root/vmmod"



Setup Boot ISO Support
----------------------

Boot ISOs are simply made by using ISO utils.

---

Install the mkisofs/isoinfo utils ::

    sudo yum install -y genisoimage


Update Firewall
---------------

Allow Attune through the firewall and port forward to the non-privileged port ::

    sudo firewall-cmd --add-forward-port=port=80:proto=tcp:toport=8000
    sudo firewall-cmd --add-forward-port=port=443:proto=tcp:toport=8001
    sudo firewall-cmd --runtime-to-permanent


Disable SELinux
---------------

The Attune service fails to write files to :file:`/home/attune/attune.home/tmp` with
SELinux enabled. ::

    sudo sed -i 's/SELINUX=enforcing/SELINUX=permissive/g' /etc/sysconfig/selinux
    sudo sed -i 's/SELINUX=enforcing/SELINUX=permissive/g' /etc/selinux/config
    sudo setenforce permissive


What Next?
----------

:ref:`install_attune`


