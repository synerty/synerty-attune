.. _tutorial_2:

========================================
Tutorial 2 - Plans, Variables And Values
========================================

Variables are used in steps, their usable in most step text fields.
They allow steps to be created that have values dynamically inserted when they are executed.

Variables come in two forms, The first form of Variable is automatically
created when a new Variable is used but not defined. They will show up in the plan as text boxes.

A text Variable is curly braces containing numbers, letters, underscores or
dots and not preceded by a dollar sign.

Here is an example: ::

    echo "=== Our first Variable ==="
    echo "adhocTextVal1 = {adhocTextVal1}"
    echo "=== Our second Variable ==="
    echo "ADHOC_TEXT_VAL_2 = {ADHOC_TEXT_VAL_2}"

In a plan, you map Variables to Values, this mapping allows you to execute
the procedure on multiple servers, with all the right values.

The other form of Variable is configured before it’s mapped in a plan.
These show up in the plan as drop down boxes.

There is greater control over these Variables, you can hide them when viewing a plan,
and they have more properties that can be accessed.

Variables themselves have no details, they are just a name and the type of Variable.

Values contain the properties that are accessed when a plan is executed.

Here is an example of accessing Values : ::

    echo "=== OS Credential Variable, Mapped to the Values in the plan ==="
    echo "administratorOnWindows.name = {administratorOnWindows.name}"
    echo "administratorOnWindows.user = {administratorOnWindows.user}"
    echo "administratorOnWindows.password = {administratorOnWindows.password}"

The step can refer to any Variable, and any property of the Value it’s mapped to.
It’s a powerful feature.

Any Variable you refer to will show up in the plan for mapping.

A plan links to a procedure and defines a Variable to Value map. Multiple plans can
be created for a procedure.

If the procedure was “Refresh Dev Server”, you may have two plans “Refresh devapp1”
and “Refresh devapp2”. Each plan would specify the relevant Variables.

Variables, Values and plans allow reusable procedures that can be perfected and then
run on multiple servers.

Factoring out values that may change such as software version numbers, passwords and
file paths are another great use of Variables.


The following names have been renamed after this tutorial was produced.

:Attune: was previously called SynBuild.
:Variables: were previously called PlaceHolders.
:Values: were previously called PlaceValues.
:Jobs: were previously called Executions.

.. raw:: html

    <embed>
        <iframe height="500pt" width="700pt" src="https://www.youtube.com/embed/Po46jupG8uU?feature=oembed"
        frameborder="0" gesture="media" allow="encrypted-media"
        allowfullscreen=""></iframe>
    </embed>

