.. _tutorial_overview:

=================
Tutorial Overview
=================

Welcome to the technical details of Attune. From here
on in we explain Attune in detail, and how to use it.
One of the biggest challenges we faced when talking about Attune is describing what it

does. Attune doesn’t do anything specific, yet it can be used for so much.
Everyones needs are different, Attune is fully configurable and structured to automate those needs.

Attune is able to execute commands and deploy files on remote servers, windows and linux
 It could even be used for configuring networking gear via SSH. Attune is fully configurable via the user interface and makes very few assumptions about what you want to do with it.

It relies on the ability to complete tasks via command line.
You will find that almost everything has command line support, such as everything in linux,
and almost everything in windows via powershell, Microsoft is extending powershells functionality with every release.

Even an oracles universal installer can be automated via command line.

Command line can take extra time to setup, but once the procedure is configured in Attune,
it’s reusable and repeatable.

Quick Overview of Attune
------------------------

The following names have been renamed after this tutorial was produced.

:Attune: was previously called SynBuild.
:Variables: were previously called PlaceHolders.
:Values: were previously called PlaceValues.
:Jobs: were previously called Executions.

.. raw:: html

    <embed>
        <iframe height="500pt" width="700pt" src="https://www.youtube.com/embed/uVkTsbRWKKQ?feature=oembed"
        frameborder="0" gesture="media" allow="encrypted-media"
        allowfullscreen=""></iframe>
    </embed>


Structure
---------

The following diagram illustrates the configuration structure of Attune.
It’s this design that allows the creation of reusable procedures.


.. image:: technical_overview.png
