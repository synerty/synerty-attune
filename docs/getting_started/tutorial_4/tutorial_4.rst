.. _tutorial_4:

=================
Tutorial 4 - Jobs
=================

A Job in Attune is needed to run a plan. Jobs display the progress and stores results.

Multiple Jobs can be created for a single plan, this allows results to be stored from
past Jobs, which can then be compared to recent Jobs, to see if things are fixed.

Plans and steps can be edited directly from the Job screen. This ease of use speeds up
the debugging process and makes creating an automated procedure nearly as fast as
doing manually.

The output from each steps Job is captured and stored with the step results,
The output from failed or individual steps is easily viewed.

Once each step is executed, Attune will check the output and exit code,
stopping the Job if there is an issue.

If a step does fail, the procedure will stop, the step can be edited from the procedure
screen and then the single step can be re-run. Or if it suits, any step in the procedure
can be run one at a time.

With out Attune, If a procedure takes 2 hours to run,
and its scripted in one large text file, there would be several problems.

The first, is that the log output is all going to one place.
There are thousands of lines of text, it takes a while to
thoroughly review the log file for errors.
It’s also very difficult to locate and view the output for a specific step.

The second, is when an error is found and fixed, the procedure needs to be restarted.
There’s another two hours.

And lastly, there is usually no error checking on each step.
The script will run end to end, snowballing the errors.
The fact is, there often isn’t enough time or funds to write a robust script.

These are the problems Attune was designed to solve.

Attune tracks the time each step takes to execute,
this allows Attune to provide some great per step progress indications.

Jobs in Attune provide more robust automation and enable fast debugging.


The following names have been renamed after this tutorial was produced.

:Attune: was previously called SynBuild.
:Variables: were previously called PlaceHolders.
:Values: were previously called PlaceValues.
:Jobs: were previously called Executions.

.. raw:: html

    <embed>
        <iframe height="500pt" width="700pt" src="https://www.youtube.com/embed/cHFs2LZw5KY?feature=oembed"
        frameborder="0" gesture="media" allow="encrypted-media"
        allowfullscreen=""></iframe>
    </embed>
