.. _tutorial_3:

=====================
Tutorial 3 - Archives
=====================

All files stored in Attune are stored as archives, even if it’s just one file.
Archives are used during procedures to deploy files, any file that’s needed.

These can be, installers, configuration files, packages or even 50 gig database dumps.
There is no limit on the size of a file in an archive.

Here are some features of the archive editor:

* Files can be edited directly in Attune, making procedures much easier and faster to create.

* New files can be created directly within Attune, with out the need for uploading one.

* Archives can be downloaded, modified using a file manager, and uploaded again.

* Content edited online is made easier with syntax highlighting.

When files are copied to a server, something needs to be done with them next.
Command steps can be added after the archive deployment, allowing complete automated procedures.

These are some of the archive deployment step types:

* Deploy Archive. Copies the contents of the archive to the remote server.
  In windows this is done via windows file sharing, for linux, this is done via SSH.

* Deploy RPM. Copies, installs and then deletes Redhat packages to/on Redhat servers.

* Deploy Template Archive. A special deployment type discussed later on.


The following names have been renamed after this tutorial was produced.

:Attune: was previously called SynBuild.
:Variables: were previously called PlaceHolders.
:Values: were previously called PlaceValues.
:Jobs: were previously called Executions.

.. raw:: html

    <embed>
        <iframe height="500pt" width="700pt" src="https://www.youtube.com/embed/w3VSAlwsgkQ?feature=oembed"
        frameborder="0" gesture="media" allow="encrypted-media"
        allowfullscreen=""></iframe>
    </embed>
