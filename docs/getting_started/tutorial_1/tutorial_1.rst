.. _tutorial_1:

=======================
Tutorial 1 - Procedures
=======================

The heart of Attune is the procedure.
These procedures are built up from a hierarchy, of small re-usable steps.

Steps and procedures are made re-usable through the use of Variables.
Variables are mapped to values later on.

Within the script areas, you can leverage all the shell commands you need.
This includes for loops, variables, if statements, etc. You can even run other interpreters,
such as perl, python, or php.

A plan maps the Variables to values. Plans configure procedures on different servers,
with different values.

Procedures are a hierarchy of steps, steps can be linked to multiple procedures.
A procedure is it’s self a step.

This means procedures build up an organised or inherited structure.

Procedures are built from different types of steps,
these steps include executing commands or SQL on remote servers,
or deploying files to remote servers. More steps are added with new releases.


The following names have been renamed after this tutorial was produced.

:Attune: was previously called SynBuild.
:Variables: were previously called PlaceHolders.
:Values: were previously called PlaceValues.
:Jobs: were previously called Executions.

.. raw:: html

    <embed>
        <iframe height="500pt" width="700pt" src="https://www.youtube.com/embed/uVkTsbRWKKQ?feature=oembed"
        frameborder="0" gesture="media" allow="encrypted-media"
        allowfullscreen=""></iframe>
    </embed>
