.. _tutorial_6:

============================
Tutorial 6 - Scheduled Tasks
============================

Attune’s Scheduler allows admins to schedule procedures for automatic Job.

Schedules have one or more scheduled times that specify when to run.
These times can be run once or recurring.

Schedules can run multiple procedures, and have support for delays between the procedures.
This feature makes coordination of resource intensive scheduled tasks across multiple servers robust.

Schedules can have multiple result actions, emails can be sent on several statuses,
mainly success, warning, failed or all.


.. image:: tutorial_6_shedules.png
