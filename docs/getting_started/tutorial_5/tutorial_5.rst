.. _tutorial_5:

===============================
Tutorial 5 - Generating Configs
===============================

Template archives contain files that need processing by the template engine. The processing and deploying is done by the
“Deploy Template Archive” step.

The archives can contain other files that don’t need processing, the step will deploy them normally.

A template engine takes a template file, processes it and renders the final contents of the file. The template file contains markup that tells the template engine what to do.

As an example, the template engine will render this: ::

    # Hosts file configured for new servers
    # Date : Today
    # Example of non rendered text
    # Host ${server.name}
    ${server.ip}    ${server.hostname} ${server.fqn}

    # Hosts for ${servers.name}
    % for otherServer in servers.servers:
    % if not otherServer == server:
    ${otherServer.ip}   ${otherServer.hostname} ${otherServer.fqn}
    % endif
    % endfor

to this ::

    # Hosts file configured for new servers
    # Date : Today
    # Example of non rendered text
    # Host ProdApp1
    10.0.0.2    prodapp2    prodapp2.synerty.com

    # Hosts for Production Servers
    10.0.0.1    prodapp1    prodapp1.synerty.com
    10.0.0.3    prodapp3    prodapp3.synerty.com


Attune uses the mako template engine, it’s based on python.

`https://www.makotemplates.org <https://www.makotemplates.org>`_

You can easily write python code, within blocks, in the template.
The template engine is seamlessly embedded in Attune allowing files to be
rendered and deployed as part of procedures.

Template files were implemented to solve the problem of deploying some configuration files.
These configuration files required one or more lines to be repeated  depending on the number
of servers in an environment. This is now easily accomplished with template
files and the server group Variable.

Template files can be used for more than just configuration files, some examples are:

* Generating scripts to deploy
* Generating SQL files
* Generating custom HTML pages that are used in deployed software
* Generating XML to feed into something during deployment
* More unimaginable possibilities


The following names have been renamed after this tutorial was produced.

:Attune: was previously called SynBuild.
:Variables: were previously called PlaceHolders.
:Values: were previously called PlaceValues.
:Jobs: were previously called Executions.

.. raw:: html

    <embed>
        <iframe height="500pt" width="700pt" src="https://www.youtube.com/embed/Szw1RPQTcFs?feature=oembed"
        frameborder="0" gesture="media" allow="encrypted-media"
        allowfullscreen=""></iframe>
    </embed>

