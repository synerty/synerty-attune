.. _first_steps:

=======================
First Steps with Attune
=======================

Create Variables
----------------

These are used in the procedures as placeholders for the real servers and credentials.
Create two variables in Attune, we’re using Linux in this example.

* Create a Windows or Linux OS Credential
* Create a Windows or Linux Server

.. image:: first_steps_variables.png

Create Values
-------------

These are the actual values for the servers and credentials.

* Create a Windows or Linux Server
* Create a Windows or Linux OS Credential

.. image:: first_steps_values.png

Create a Procedure
------------------

A procedure contains all the scripts and steps to run.

Create the procedure step :

.. image:: first_steps_procedure_1.png

Create the shell step :

.. image:: first_steps_procedure_2.png

Create a Plan
-------------

Plans map the procedure, variables and values together.

.. image:: first_steps_plan_1.png

Once you click create, you can then enter the mapping for the values :

.. image:: first_steps_plan_2.png

Create a Job
------------

Finally, Create and run a job. A job is the running of a plan.

.. image:: first_steps_job_1.png

Now you can run the job

.. image:: first_steps_job_2.png

Complete
--------

You’ve just automated your own procedure, making it easy for the whole team to run your script.

