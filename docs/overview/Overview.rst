.. _overview:

========
Overview
========

Attune is a system that allows you to run scripts and commands, and deploy files,
on remote servers that run Windows
or Linux. A lot of systems administration tasks that make use of scripts, can be
automated with Attune. Attune provides
facilities that allow you to create procedures that are more modular and re-usable
than normal scripts.

Procedures
----------

Attune's procedures allow you to execute commands and scripts written in Bash,
Perl, Python, and Windows Powershell
on different machines while still being held centrally for consistency and ease of
maintenance. Attune has a built-in
editor which allows you to create and edit files from within Attune. Attune
procedures can deploy files
and execute SQL procedures on remote servers.

Variables
---------

Attune has Variables which allow you to reference generic placholder variables
in your scripts. You can assign
different Values to the Variable variables that are used in Procedure
scripts. This is done in a Plan, and
allows you to create Procedures that are generic and re-usable. A procedure can
have multiple plans - each assigning
different values to the variables.

Jobs
----

Attune has Jobs, which help to make debugging Procedures quicker and easier.
The Jobs track the output and
allows you to easily see where procedure steps have failed. You can also make
changes to the scripts, test changes, and
resume job of a procedure at the point where it had failed, all from within
an Attune Job. This saves having
to restart a script from the beginning if it fails.

Archives
--------

Attune has Archives, which allow you to store files centrally and deploy them on a
number of different remote servers.
These Archives are deployed by special procedure steps in Attune. Attune also
allows you to deploy template archives, which
make use of the Mako template engine - an embedded Python language, which provide
further power and flexibility.

Scheduling
----------

Attune has a powerful scheduling system which allows you to set up procedures to
run at specified dates and times, or
run repeatedly at a specified time interval. You can set a number of different
procedures to execute at a particular
time interval, and have the option to selectively disable or enable particular
procedures as required.

